import pygame
import random
pygame.init()

""" Constant definitions these include:
colours, game display sizes, font sizes, block sizes, FPS the game runs at, all the aditional fruits"""

WHITE =(255,255,255)
BLACK = (0,0,0)
RED = (255,0,0)
GREEN = (0,155,0)
YELLOW = (255,255,0)
DARK_GREEN = (0,100,0)
ORANGE = (255,140,0)
PINK = (255,192,203)

DISPLAY_WIDTH = 800
DISPLAY_HEIGHT = 600

GAME_DISPLAY = pygame.display.set_mode((DISPLAY_WIDTH,DISPLAY_HEIGHT))
pygame.display.set_caption("SnakePlusPlus")

clock = pygame.time.Clock()

small_font = pygame.font.SysFont("comicsansms",15)
med_font = pygame.font.SysFont("comicsansms",40)
large_font = pygame.font.SysFont("comicsansms",60)
BLOCK_SIZE = 10
FRAMES_PER_SECOND = 10

FRUITS  = {1: "lemon", 2: "water_melon", 3: "strawberry", 4 : "orange", 5 : "none", 6: "none"
           , 7: "none", 8: "none", 9: "none", 10: "none"}


def game_intro():
    """ This is the game intro screen procedure it requires no parameters to be passed to it
    this function handles creation and display of the intro scrren that is first shown
    when the game is fist started and displays instructions for the game """
    
    #This loops runs the event handling in the games introduction screen 
    intro = True
    while intro:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    intro = False
                if event.key == pygame.K_q:
                    pygame.quit()
                    quit()
                if event.key == pygame.K_ESCAPE:
                    GAME_DISPLAY.fill(WHITE)
                    pygame.display.update()
                    pause_screen()
                    
        #This block of code is used to call the message-to-screen function multiple times
        #To display infomation for the player
        GAME_DISPLAY.fill(WHITE)
        message_to_screen("Welcome to SnakePlusPlus", GREEN, -100, size ="large")
        message_to_screen("The objective of this game is to collect the red apples",BLACK, -30)
        message_to_screen("The more apples eaten the longer you get",BLACK, 10)
        message_to_screen("If you colide with your own body or the side of the screen you will loose",BLACK, 90)
        message_to_screen("Use arrow keys or WASD keys tomove",BLACK, 50)
        message_to_screen("Press Space to play, Esc to pause, or Q to quit",BLACK, 180)
        
        #Used to update the display and set the FPS for the screen
        pygame.display.update()
        clock.tick(FRAMES_PER_SECOND)

def pause_screen():
    """ This is the pause screen it requires no parameters to be passed to it
    this procedure handles the creation and display of the pause screen after the
    player has pressed the escape key it gives players the option of
    quiting, continuing or starting a new game """
    
    #This loops runs the event handling in the games pause screen 
    pause = True
    while pause:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    pause = False
                if event.key == pygame.K_n:
                    game_loop()
                if event.key == pygame.K_q:
                    pygame.quit()
                    quit()
                    
        #This block of code is used to call the message-to-screen function multiple times
        #To display infomation for the player
        message_to_screen("Game paused", GREEN, -100, size ="large")
        message_to_screen("Press Space to continue this game",BLACK, -30)
        message_to_screen("Press N to start a new game ",BLACK, 10)
        message_to_screen("Press Q to quit ",BLACK, 50)
        
        #Used to update to display and set the FPS for the screen
        pygame.display.update()
        clock.tick(FRAMES_PER_SECOND)

def game_over_screen(snake_length):
    """ This is the game over screen screen it requires no parameters to be passed to it
    this procedure handles the creation and display of the game over screen after the
    player has triggered a game over state it gives the player the options of
    quiting, or starting a new game snake length is the amount of elements in the
    3-D array snake_list - 1 for the head"""

    #This loops runs the event handling in the games pause screen
    game_over = True
    while game_over:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        game_loop()
                    if event.key == pygame.K_q:
                        pygame.quit()
                        quit()
        
        GAME_DISPLAY.fill(WHITE)
        #This block of code is used to call the message-to-screen function multiple times
        #To display infomation for the player
        message_to_screen ("Game over", RED, -50, size = "large")
        message_to_screen ("press space to play again or Q to quit", BLACK, 50, size = "medium")
        score(snake_length-1)
        #This loops runs the event handling in the games pause screen
        pygame.display.update()
        clock.tick(FRAMES_PER_SECOND)

def text_objects(text, color, size):
    """ This function is used in conjunction with the
    message_to_screen procedure in order to create and correctly display text
    to the screen this fuction handles the creation """
    
    if size == "small":
        text_surface = small_font.render(text, True, color)
    elif size == "medium":
        text_surface = med_font.render(text, True, color)
    elif size == "large":
        text_surface = large_font.render(text, True, color)
    return text_surface, text_surface.get_rect()

def message_to_screen(msg, color, y_displacement=0, size = "small"):
    """ This procedure is used in conjunction with the
    message_to_screen procedure in order to create and correctly display text
    to the screen this procedure handles the displaying """
        
    text_surface, text_rectangle = text_objects(msg,color, size)
    text_rectangle.center = (DISPLAY_WIDTH / 2), (DISPLAY_HEIGHT / 2)+y_displacement
    GAME_DISPLAY.blit(text_surface, text_rectangle)

def co_ord_gen():
    """ This function requires no parameters to be passed to it is used to generate
    two random coordinates to be used in the creation of fruits """
    
    rand_x = round (random.randrange (0, DISPLAY_WIDTH-BLOCK_SIZE)/10.0)*10.0
    rand_y = round (random.randrange (0, DISPLAY_HEIGHT-BLOCK_SIZE)/10.0)*10.0
    return rand_x, rand_y

def score(score):
    """This procedure handles the displaying of score
    in the upper left corner of the screen score is the length of the snake -1 """

    score *= 5
    text = small_font.render("Score: " + str(score), True, BLACK)
    GAME_DISPLAY.blit(text, [0,0])

def snake(snake_list):
    """ This procedure requires the snake list, list to be passed to it
    using this list it handles the drawing of the snake onto the screen
    snake_list is the 3-D list with the coordinates of the snake blocks
    in it """
    
    #This iterates through the 3-D array and draws a block for each set of coordinates in the list 
    for x_and_y in snake_list:
        pygame.draw.rect(GAME_DISPLAY, GREEN , [x_and_y[0],x_and_y [1],BLOCK_SIZE,BLOCK_SIZE])

def apple_collision(lead_x, lead_y, rand_apple_x, rand_apple_y, snake_length,rand_num):
    """ This fucntion is used to check if the snake has colided with the apple
    it then applies the effect of the apple to the snake by adding a block to
    snake list and editing the score
    lead x is the x coordinate of the snake head lead_y is the y coordinate
    rand_apple_x is the x coordiantae for tha apple rand_apple_y is the y
    snake_length is the ammount of elements in snake list  
    """
    
    #This block checks to see if the snake has eaten the apple then applies the effects of apple to the snake
    if lead_x == rand_apple_x and lead_y == rand_apple_y:
        rand_apple_x, rand_apple_y = co_ord_gen()
        snake_length += 1
        rand_num = random.randint(1,10)
    return rand_apple_x, rand_apple_y, snake_length, rand_num

def lemon_collision(lead_x, lead_y, rand_lemon_x, rand_lemon_y, snake_length, rand_num, snake_list, draw_lemon):
    """ This function is used to check if the snake has colided with the lemon
    it then applies the effect of the lemon to the snake by poping the last block
    off the snake list and edits the score
    lead x is the x coordinate of the snake head lead_y is the y coordinate
    rand_lemon_x is the x coordiantae for the lemon rand_lemon_y is the y
    snake_length is the ammount of elements in snake list
    snake_list is the 3-D array containg snake coordinates
    rand_num is the key for the FRUITS dictonary
    draw lemon is the Boolean that controls if a lemon needs to be drawn
    """

    if draw_lemon == True:
        
        #Used to draw a lemon to the screen 
        pygame.draw.rect(GAME_DISPLAY, YELLOW , [rand_lemon_x, rand_lemon_y, BLOCK_SIZE, BLOCK_SIZE])
        
        #This block checks to see if the snake has eaten the lemon then applies the effects of lemon to the snake
        if lead_x == rand_lemon_x and lead_y == rand_lemon_y:
            rand_lemon_x, rand_lemon_y = co_ord_gen()
            #sets the random number back to a key that does not generate a fruit
            rand_num = 10
            snake_length -= 1
            snake_list.pop()
            draw_lemon = False
    return rand_lemon_x, rand_lemon_y, snake_length, rand_num, snake_list, draw_lemon

def water_melon_collision(lead_x, lead_y, rand_water_melon_x, rand_water_melon_y, snake_length, rand_num, draw_water_melon):
    """ This function is used to check if the snake has colided with the water melon
    it then applies the effect of the water melon to the snake by adding two to
    the snake length this automaticaly updates the score
    lead x is the x coordinate of the snake head lead_y is the y coordinate
    rand_water_melon_x is the x coordiantae for tha water melon rand_water_melon_y is the y
    snake_length is the ammount of elements in snake list
    rand_num is the key for the FRUITS dictonary
    draw water_melon is the Boolean that controls if a orange needs to be drawn
    """
    
    if draw_water_melon == True:
        
        #Used to draw a water melon to the screen 
        pygame.draw.rect(GAME_DISPLAY, DARK_GREEN , [rand_water_melon_x, rand_water_melon_y, BLOCK_SIZE, BLOCK_SIZE])
        
        #This block checks to see if the snake has eaten the water melon then applies the effects of water melon to the snake
        if lead_x == rand_water_melon_x and lead_y == rand_water_melon_y:
            rand_water_melon_x, rand_water_melon_y = co_ord_gen()
            #sets the random number back to a key that does not generate a fruit
            rand_num = 10
            snake_length += 2
            draw_water_melon = False
    return rand_water_melon_x, rand_water_melon_y, snake_length, rand_num, draw_water_melon

def strawberry_collision(lead_x, lead_y, rand_strawberry_x, rand_strawberry_y, snake_length, rand_num, snake_list, draw_strawberry):
    """ This function is used to check if the snake has colided with the strawberry
    it then applies the effect of the strawberry to the snake by poping the two last blocks
    off the snake list and edits the score
    lead x is the x coordinate of the snake head lead_y is the y coordinate
    rand_strawberry_x is the x coordiantae for the strawberry rand_strawberry_y is the y
    snake_length is the ammount of elements in snake list
    snake_list is the 3-D array containg snake coordinates
    rand_num is the key for the FRUITS dictonary
    draw strawberry is the Boolean that controls if a strawberry needs to be drawn
    """

    if draw_strawberry == True:
        
        #Used to draw a lemon to the screen 
        pygame.draw.rect(GAME_DISPLAY, PINK , [rand_strawberry_x, rand_strawberry_y, BLOCK_SIZE, BLOCK_SIZE])
        
        #This block checks to see if the snake has eaten the lemon then applies the effects of lemon to the snake
        if lead_x == rand_strawberry_x and lead_y == rand_strawberry_y:
            rand_strawberry_x, rand_strawberry_y = co_ord_gen()
            #sets the random number back to a key that does not generate a fruit
            rand_num = 10
            snake_length -= 2
            snake_list.pop()
            snake_list.pop()
            draw_strawberry = False
    return rand_strawberry_x, rand_strawberry_y, snake_length, rand_num, snake_list, draw_strawberry


def orange_collision (lead_x, lead_y, rand_orange_x, rand_orange_y, snake_length, rand_num, draw_orange):
    """ This function is used to check if the snake has colided with the orange
    it then applies the effect of the orange to the snake by adding three to
    the snake length this automaticaly updates the score
    lead x is the x coordinate of the snake head lead_y is the y coordinate
    rand_orange_x is the x coordiantae for the orange rand_orange_y is the y
    snake_length is the ammount of elements in snake list
    rand_num is the key for the FRUITS dictonary
    draw orange is the Boolean that controls if a orange needs to be drawn
    """
    
    if draw_orange == True:
        
        #Used to draw a water melon to the screen 
        pygame.draw.rect(GAME_DISPLAY, ORANGE , [rand_orange_x, rand_orange_y, BLOCK_SIZE, BLOCK_SIZE])
        
        #This block checks to see if the snake has eaten the water melon then applies the effects of water melon to the snake
        if lead_x == rand_orange_x and lead_y == rand_orange_y:
            rand_orange_x, rand_orange_y = co_ord_gen()
            #sets the random number back to a key that does not generate a fruit
            rand_num = 10
            snake_length += 3
            draw_orange = False
    return rand_orange_x, rand_orange_y, snake_length, rand_num, draw_orange
    
    
    

def game_loop():
    """ This is the main game loop it handles events while the program is
    in the state where the player is in control it is also used to call
    the other functions and procedures at certain events """

    #This block of code is used to define definitions that will be used in the game loop
    game_exit = False
    game_over = False

    lead_x = DISPLAY_WIDTH/2
    lead_y = DISPLAY_HEIGHT/2

    lead_x_change = 0
    lead_y_change = 0
    current_direction = ()

    snake_list = []
    snake_length = 1


    rand_num = 10

    #Booleans that control the draawing of fruits
    draw_lemon = False
    draw_water_melon = False
    draw_strawberry = False
    draw_orange = False
    
    #This section of code gerentaes random cordinates for the fruits
    rand_orange_x, rand_orange_y = co_ord_gen ()
    rand_strawberry_x, rand_strawberry_y = co_ord_gen()
    rand_water_melon_x, rand_water_melon_y = co_ord_gen()    
    rand_lemon_x, rand_lemon_y = co_ord_gen()
    rand_apple_x, rand_apple_y = co_ord_gen()


    #This loop runs untill the game_exit boolean is changed to true 
    while not game_exit:

        #This will call the game_over_screen when a game_over_state is met
        while game_over == True:
            game_over_screen(snake_length)

        #This block is used for event handling player movement 
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_exit = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT and current_direction != 1 or event.key == pygame.K_a and current_direction != 1:
                    lead_x_change = -BLOCK_SIZE
                    lead_y_change = 0
                    current_direction = 0
                elif event.key == pygame.K_RIGHT and current_direction != 0 or event.key == pygame.K_d and current_direction != 0:
                    lead_x_change = BLOCK_SIZE
                    lead_y_change = 0
                    current_direction = 1
                elif event.key == pygame.K_UP and current_direction != 3 or event.key == pygame.K_w and current_direction != 3:
                    lead_y_change = -BLOCK_SIZE
                    lead_x_change = 0
                    current_direction = 2
                elif event.key == pygame.K_DOWN  and current_direction != 2 or event.key == pygame.K_s and current_direction != 2:
                    lead_y_change = BLOCK_SIZE
                    lead_x_change = 0 
                    current_direction = 3
                elif event.key == pygame.K_ESCAPE:
                    pause_screen()

        #This statement is used to check that the snake stays within the boundries of the screen 
        if lead_x >= DISPLAY_WIDTH or lead_x < 0 or lead_y >= DISPLAY_HEIGHT or lead_y < 0:
            game_over = True

        #These variabeles are used to keeep the snake in motion and syop diagnal movement
        lead_x += lead_x_change
        lead_y += lead_y_change

        #This block is used fill the screen then draw the apple over the top of the screen
        GAME_DISPLAY.fill(WHITE)
        pygame.draw.rect(GAME_DISPLAY, RED , [rand_apple_x, rand_apple_y, BLOCK_SIZE, BLOCK_SIZE])

        #This block of code is used to create a 3-D array within the the snake_list list
        snake_head = []
        snake_head.append(lead_x)
        snake_head.append(lead_y)
        snake_list.append(snake_head)


        #This block of code is used to make it so thay the snake stays are the correct length
        if len (snake_list) > snake_length:
            del snake_list [0]

        #This block of code is used to meake sure the head of the snake has not colided with the tail
        for each_segment in snake_list[:-1]:
            if each_segment == snake_head:
                game_over = True

        #This checks to make sure that the snake length is always more than or equal to zero to make sure the snake is always visable
        if snake_length <= 0:
            game_over_screen(snake_length)

        #This is used to call the procedure that draws the snake to the screen 
        snake(snake_list)

        #This is used to call the procedure that calculates and displays the score 
        score(snake_length-1)

        #This is used to check if the snake head has come into conatct with an apple
        rand_apple_x, rand_apple_y, snake_length, rand_num = apple_collision(lead_x, lead_y, rand_apple_x, rand_apple_y, snake_length, rand_num)
        
        #This block of if statements is used to generate and check the colision of the othe fruits
        if FRUITS [rand_num] == "lemon":
             draw_lemon = True
        if draw_lemon == True:
            rand_lemon_x, rand_lemon_y, snake_length, rand_num, snake_list, draw_lemon = lemon_collision(lead_x, lead_y, rand_lemon_x, rand_lemon_y,
                                                                                                         snake_length, rand_num, snake_list, draw_lemon)
            
        if FRUITS [rand_num] == "water_melon":
            draw_water_melon = True
        if draw_water_melon == True:
            rand_water_melon_x, rand_water_melon_y, snake_length, rand_num, draw_water_melon = water_melon_collision(lead_x, lead_y,
                                                                                                                     rand_water_melon_x,rand_water_melon_y,
                                                                                                                     snake_length, rand_num, draw_water_melon)
        if FRUITS [rand_num] == "strawberry":
            draw_strawberry = True
        if draw_strawberry == True:
            rand_strawberry_x, rand_strawberry_y, snake_length, rand_num, snake_list, draw_strawberry = strawberry_collision(lead_x, lead_y, rand_strawberry_x, rand_strawberry_y,
                                                                                                         snake_length, rand_num, snake_list, draw_strawberry)
            

        if FRUITS [rand_num] == "orange":
            draw_orange = True
        if draw_orange == True:
            rand_orange_x, rand_orange_y, snake_length, rand_num, draw_orange = orange_collision( lead_x, lead_y, rand_orange_x, rand_orange_y, snake_length,
                                                                                                  rand_num, draw_orange)
    
                                                    
        pygame.display.update()
        clock.tick(FRAMES_PER_SECOND)

    pygame.quit()
    quit() 
        
game_intro()
game_loop()

if __name__ == "__main__":
    main()
